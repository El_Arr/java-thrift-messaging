package com.el_arr.thrift;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

public class AnswerServiceClient {

	private static void client(AnswerService.Client client) throws TException {
		if (client.ping()) {
			int key = 4;
			client.key(key);
			String[] messages = {
					"The Ultimate Question of Life, the Universe, and Everything.",
					"Hello there.", "It's over Anakin! I have the high ground.", "Invoking error message."
			};
			for (int i = 0; i < messages.length; i++) {
				String message = messages[i];
				System.out.println("Message: " + message);
				String encrypted = AnswerMessageEncryption.encrypt(message, key);
				System.out.println("Encrypted message: " + encrypted);
				String response = "";
				try {
					response = client.answer(new AnswerMessage(i, encrypted)).data;
				} catch (UnanswerableException e) {
					System.out.println("Error: Question Unanswerable.");
				}
				if (!response.isEmpty()) {
					System.out.println("Response: " + response);
					String decrypted = AnswerMessageEncryption.decrypt(response, key);
					System.out.println("Decrypted response: " + decrypted + "\n");
				}
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} else {
			System.out.println("Failed to connect to server.");
		}
	}

	public static void main(String[] args) throws TException {
		final int PORT = 9090;
		String host = "localhost";
		if (args.length != 0) host = args[0];

		TTransport transport = new TSocket(host, PORT);
		transport.open();

		TProtocol protocol = new TBinaryProtocol(transport);
		AnswerService.Client client = new AnswerService.Client(protocol);

		client(client);

		transport.close();
	}

}

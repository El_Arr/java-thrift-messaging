package com.el_arr.thrift;

public class AnswerMessageEncryption {

	public static String decrypt(String message, int key) {
		return crypt(message, key, false);
	}

	public static String encrypt(String message, int key) {
		return crypt(message, key, true);
	}

	public static String crypt(String message, int key, boolean encrypt) {
		int mode = encrypt ? 1 : -1;
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < message.length(); ++i)
			result.append((char) (((int) message.charAt(i)) + key * mode));
		return result.toString();
	}

}
